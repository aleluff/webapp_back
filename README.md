### Requirement

- node
- postman (desktop)
- docker

### Setup

``` sh
docker-compose up -d

cp .env.development .env

npm install
```

Go to [PhpMyAdmin](http://localhost:8081), and execute static/init.sql

``` sh
npm run dev
```

TODO :<br>
https://medium.com/@jackrobertscott/how-to-use-google-auth-api-with-node-js-888304f7e3a0
