require('dotenv').config();

const express = require("express");
const cors = require("cors");
const fsp = require('fs').promises;
const i18n = require('./utils/i18n')
const app = express();
const port = Number(process.env.API_PORT);

app.use(i18n.init)
app.use(express.json());
app.use(cors({origin: '*'}));
app.use(function (req, res, next) {
  res.error = function (status, i18nKey, params) {
    res.status(status).json({
      ["error"]: [{msg: i18n.__(i18nKey, params)}]
    });
  };
  next();
});

(async () => {
  const objectPath = __dirname + "/objects";
  const entries = await fsp.readdir(objectPath);
  for (const entry of entries) {
    app.use(`/api/${entry}`, require(`${objectPath}/${entry}/route`));
  }

  app.all('*', (req, res) => {
    res.error(404, 'global.endpoint_not_found');
  });

  app.listen(port, () => console.log(`🚀 Server running on port ${port}!`));
})();
