const SensorModel = require('./model');

class Controller {
  getAllSensors = async (req, res) => {
    res.send(await SensorModel.find({id_user: req.currentUser.id}));
  };

  createSensor = async (req, res) => {
    req.body.id_user = req.currentUser.id;
    req.body.attribution_date = new Date();

    const result = await SensorModel.create(req.body);
    if (!result) {
      return res.error(500, 'global.something_wrong');
    }

    res.send();
  };

  attachSensor = async (req, res) => {
    const sensor = await SensorModel.findOne({id: req.params.id});
    if (!sensor) {
      return res.error(400, 'sensor.does_not_exist');
    }

    req.body.attribution_date = new Date()

    const result = await SensorModel.update({id_user: req.currentUser.id}, req.params.id);
    if (!result) {
      return res.error(500, 'global.something_wrong');
    }

    res.send();
  };

  detachSensor = async (req, res) => {
    const sensor = await SensorModel.findOne({id: req.params.id});
    if (!sensor) {
      return res.error(400, 'sensor.does_not_exist');
    }
    if (sensor.id_user !== req.currentUser.id) {
      return res.error(400, 'user.not_authorized');
    }

    const result = await SensorModel.update({id_user: null}, req.params.id);
    if (!result) {
      return res.error(500, 'global.something_wrong');
    }

    res.send();
  };
}

module.exports = new Controller;
