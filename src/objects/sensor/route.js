const express = require('express');
const router = express.Router();
const sensorController = require('./controller');
const auth = require('../../middleware/auth');
const validate = require('../../middleware/validate');
const {createSensorSchema, updateSensorSchema} = require('./validator');

router.get('/', auth(), sensorController.getAllSensors);
router.post('/', validate(createSensorSchema), auth(), sensorController.createSensor);
router.put('/attach/:id', validate(updateSensorSchema), auth(), sensorController.attachSensor);
router.put('/detach/:id', validate(updateSensorSchema), auth(), sensorController.detachSensor);

module.exports = router;
