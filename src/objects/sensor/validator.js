const {body, param} = require('express-validator');
const i18n = require('../../utils/i18n')

const sensorIdMinChars = { min: 6 }

exports.createSensorSchema = [
  body('id')
    .exists()
    .withMessage(i18n.__("sensor.serial_required"))
    .notEmpty()
    .withMessage(i18n.__("sensor.serial_filled"))
    .isLength(sensorIdMinChars)
    .withMessage(i18n.__("global.minimum_chars_length_required", sensorIdMinChars)),
  body('id_sensor_type')
    .exists()
    .withMessage(i18n.__("sensor.sensor_type_required"))
    .isNumeric()
    .notEmpty()
    .withMessage(i18n.__("sensor.select_sensor_type_required")),
];

exports.updateSensorSchema = [
  param('id')
    .exists()
    .withMessage(i18n.__("sensor.serial_required"))
    .notEmpty()
    .withMessage(i18n.__("sensor.serial_filled"))
    .isLength(sensorIdMinChars)
    .withMessage(i18n.__("global.minimum_chars_length_required", sensorIdMinChars))
];
