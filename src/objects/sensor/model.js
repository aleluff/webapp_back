const CommonModel = require('../../utils/common.model');

class Model extends CommonModel {
  tableName = 'sensor';

  create = async ({id, id_sensor_type, id_user, attribution_date}) => {
    const sql = `INSERT INTO ${this.tableName}
        (id, id_sensor_type, id_user, attribution_date) VALUES (?, ?, ?, ?)`;

    const result = await this.bdd.query(sql, [id, id_sensor_type, id_user, attribution_date]);
    const affectedRows = result ? result.affectedRows : 0;

    return affectedRows;
  }
}

module.exports = new Model;
