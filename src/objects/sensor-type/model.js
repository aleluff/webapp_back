const CommonModel = require('../../utils/common.model');

class Model extends CommonModel {
  tableName = 'sensor_type';
}

module.exports = new Model;
