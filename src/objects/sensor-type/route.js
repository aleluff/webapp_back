const express = require('express');
const router = express.Router();
const sensorTypeController = require('./controller');
const auth = require('../../middleware/auth');

router.get('/', auth(), sensorTypeController.getAllSensorTypes);

module.exports = router;
