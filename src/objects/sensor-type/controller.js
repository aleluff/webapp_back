const SensorTypeModel = require('./model');

class Controller {
  getAllSensorTypes = async (req, res) => {
    res.send(await SensorTypeModel.find());
  }
}

module.exports = new Controller;
