const UserModel = require('./model');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const transporter = require('../../utils/mailer');
const Roles = require('./enum');
const {passwordRegex, passwordMinLength} = require("../../utils/helpers");
const i18n = require('../../utils/i18n')

class Controller {

  getAllUsers = async (req, res) => {
    let userList = await UserModel.find();

    userList = userList.map(user => {
      const {password, ...userWithoutPassword} = user;
      return userWithoutPassword;
    });

    res.send(userList);
  };

  getUserById = async (req, res) => {
    if (req.currentUser.role !== Roles.Admin &&
      parseInt(req.params.id) !== req.currentUser.id) {
      return res.error(400, 'user.not_authorized');
    }

    const user = await UserModel.findOne({id: req.params.id});
    if (!user) {
      return res.error(404, 'user.not_found');
    }

    const {password, ...userWithoutPassword} = user;

    res.send(userWithoutPassword);
  };

  createUser = async (req, res) => {
    const user = await UserModel.findOne({email: req.body.email});
    if (user) {
      return res.error(400, 'user.already_registered');
    }

    req.body.password = await this.hashPassword(req.body.password);

    //Création du token unique de validation par mail
    const token = (Math.random() + 1).toString(36).substring(7);
    req.body.token = token;

    const result = await UserModel.create(req.body);
    if (!result) {
      return res.error(500, 'global.something_wrong');
    }

    res.send(await UserModel.findOne({email: req.body.email}));

    transporter.sendMail({
      from: '"LR " <libellien03@lrtechnologies.fr>',
      to: req.body.email,
      subject: i18n.__("user.request_to_validate_account_email_subject"),
      html: i18n.__("user.request_to_validate_account_email_content", {host: req.headers.host, token: token})
    }, function (error) {
      if (error) {
        return console.log(error);
      }
    });
  };

  //vérification du token unique => verified = 1
  getTokenValidate = async (req, res) => {
    const user = await UserModel.findOne({token: req.query.token});
    if (!user) {
      return res.error(404, 'user.not_found');
    }

    const result = await UserModel.update({user_verified: 1, token: ''}, user.id);
    if (!result) {
      return res.error(500, 'global.something_wrong');
    }

    res.send();

    //envoie du mail de confirmation
    transporter.sendMail({
      from: '"LR " <libellien03@lrtechnologies.fr>',
      to: user.email,
      subject: i18n.__("user.account_validated_email_subject"),
      html: i18n.__("user.account_validated_email_content")
    }, function (error, info) {
      if (error) {
        return console.log(error);
      }
    });
  }

  genPass() {
    let stringInclude = '';
    stringInclude += "!\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~";
    stringInclude += '0123456789';
    stringInclude += 'abcdefghijklmnopqrstuvwxyz';
    stringInclude += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let password = '';
    for (let i = 0; i < passwordMinLength; i++) {
      password += stringInclude.charAt(Math.floor(Math.random() * stringInclude.length));
    }
    return password;
  }

  reset = async (req, res) => {
    let password;

    //on identifie le user
    const user = await UserModel.findOne({email: req.body.email});
    if (!user) {
      return res.error(404, 'user.not_found');
    }

    //on génère un pwd sécurisé
    do {
      password = this.genPass();
    }
    while (!passwordRegex.test(password))
    const r = password;

    //on hash le pass
    password = await this.hashPassword(password);

    //on met à jour le pwd
    const result = await UserModel.update({password: password}, user.id);
    if (!result) {
      return res.error(500, 'global.something_wrong');
    }

    res.send();

    //on envoie le mail avec le nouveau password
    transporter.sendMail({
      from: '"LR " <libellien03@lrtechnologies.fr>',
      to: user.email,
      subject: i18n.__("user.account_validated_email_subject"),
      html: i18n.__("user.new_password_email_content", {r})
    }, function (error, info) {
      if (error) {
        return console.log(error);
      }
    });
  }

  updateUser = async (req, res) => {
    const {id, ...bodyWithoutId} = req.body;

    if (req.currentUser.role !== Roles.Admin &&
      parseInt(id) !== req.currentUser.id) {
      return res.error(400, 'user.not_authorized');
    }

    if (req.body.password) {
      bodyWithoutId.password = await this.hashPassword(req.body.password);
    }

    const result = await UserModel.update(bodyWithoutId, id);
    if (!result) {
      return res.error(500, 'global.something_wrong');
    }
    if (!result.affectedRows) {
      return res.error(404, 'user.not_found');
    }

    res.send(true);
  };

  deleteUser = async (req, res) => {
    const result = await UserModel.delete(req.params.id);
    if (!result) {
      return res.error(404, 'user.not_found');
    }

    res.send();
  };

  userLogin = async (req, res) => {
    const {email, password: pass} = req.body;

    const user = await UserModel.findOne({email});
    if (!user) {
      return res.error(404, 'user.not_found');
    }

    const isMatch = await bcrypt.compare(pass, user.password);
    if (!isMatch) {
      return res.error(400, 'user.incorrect_password');
    }

    // user matched!
    const nbDays = 365;
    const {password, ...userWithoutPassword} = user;
    const secretKey = process.env.SECRET_JWT || "";
    const token = jwt.sign({user_id: user.id.toString()}, secretKey, {
      expiresIn: nbDays + 'd'
    });
    const tokenExpiration = new Date();
    tokenExpiration.setDate((new Date()).getDate() + nbDays);

    res.send({...userWithoutPassword, token, tokenExpiration});
  };

  // hash password if it exists
  hashPassword = async (password) => {
    return await bcrypt.hash(password, 8);
  }
}

module.exports = new Controller;
