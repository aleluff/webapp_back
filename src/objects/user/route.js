const express = require('express');
const router = express.Router();
const userController = require('./controller');
const auth = require('../../middleware/auth');
const Role = require('./enum');
const validate = require('../../middleware/validate');
const {createUserSchema, updateUserSchema, validateLogin} = require('./validator');

router.get('/', auth(Role.Admin), userController.getAllUsers);
router.get('/id/:id', auth(), userController.getUserById);
router.get('/token', userController.getTokenValidate);
router.post('/reset', userController.reset);
router.post('/login', validate(validateLogin), userController.userLogin);
router.post('/', validate(createUserSchema), userController.createUser);
router.patch('/', validate(updateUserSchema), auth(), userController.updateUser);
router.delete('/id/:id', auth(Role.Admin), userController.deleteUser);

module.exports = router;
