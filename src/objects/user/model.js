const CommonModel = require('../../utils/common.model');

class Model extends CommonModel {
  tableName = 'user';

  create = async ({password, first_name, last_name, email, role, birth_date, token}) => {
    const sql = `INSERT INTO ${this.tableName}
        (password, first_name, last_name, email, role, birth_date, token) VALUES (?,?,?,?,?,?,?)`;

    const result = await this.bdd.query(sql, [password, first_name, last_name, email, role, birth_date, token]);
    return result ? result.affectedRows : 0;
  }
}

module.exports = new Model;
