const {body} = require('express-validator');
const {passwordRegex, passwordMinLength} = require("../../utils/helpers");
const i18n = require('../../utils/i18n')

const firstNameMinChars = { min: 1 }
const lastNameMinChars = { min: 1 }
const passwordMinChars = { min: passwordMinLength }

exports.createUserSchema = [
  body('first_name')
    .exists()
    .withMessage(i18n.__("user.first_name_required"))
    .matches(/^[a-zA-Z\s]*$/)
    .withMessage(i18n.__("global.alphabetical_chars_required"))
    .isLength(firstNameMinChars)
    .withMessage(i18n.__("global.minimum_chars_length_required", firstNameMinChars)),
  body('last_name')
    .exists()
    .withMessage(i18n.__("user.last_name_required"))
    .matches(/^[a-zA-Z\s]*$/)
    .withMessage(i18n.__("global.alphabetical_chars_required"))
    .isLength(firstNameMinChars)
    .withMessage(i18n.__("global.minimum_chars_length_required", lastNameMinChars)),
  body('email')
    .exists()
    .withMessage(i18n.__("user.email_required"))
    .isEmail()
    .withMessage(i18n.__("user.valid_email_required"))
    .normalizeEmail(),
  body('password')
    .exists()
    .withMessage(i18n.__("user.password_required"))
    .notEmpty()
    .matches(passwordRegex)
    .withMessage(i18n.__("user.password_constraints", passwordMinChars)),
  body('birth_date')
    .optional()
    .isNumeric()
    .withMessage(i18n.__("global.date_required"))
];

exports.updateUserSchema = [
  body('id')
    .exists()
    .isNumeric()
    .withMessage(i18n.__("global.numeric_required")),
  body('first_name')
    .optional()
    .matches(/^[a-zA-Z\s]*$/)
    .withMessage(i18n.__("global.alphabetical_chars_required"))
    .isLength({min: 1})
    .withMessage(i18n.__("global.minimum_chars_length_required")),
  body('last_name')
    .optional()
    .matches(/^[a-zA-Z\s]*$/)
    .withMessage(i18n.__("global.alphabetical_chars_required"))
    .isLength({min: 1})
    .withMessage(i18n.__("global.minimum_chars_length_required")),
  body('email')
    .optional()
    .isEmail()
    .withMessage(i18n.__("user.valid_email_required"))
    .normalizeEmail(),
  body('password')
    .optional()
    .notEmpty()
    .matches(passwordRegex)
    .withMessage(i18n.__("user.password_constraints", passwordMinChars)),
  body('birth_date')
    .optional()
    .isNumeric()
    .withMessage(i18n.__("global.date_required")),
  body()
    .custom(value => {
      return !!Object.keys(value).length;
    })
    .withMessage(i18n.__("global.fields_required_to_update"))
    .custom(value => {
      const updates = Object.keys(value);
      const allowUpdates = ['password', 'email', 'first_name', 'last_name', 'birth_date', 'id'];
      return updates.every(update => allowUpdates.includes(update));
    })
    .withMessage(i18n.__("global.invalid_update"))
];

exports.validateLogin = [
  body('email')
    .exists()
    .withMessage(i18n.__("user.email_required"))
    .isEmail()
    .withMessage(i18n.__("user.valid_email_required"))
    .normalizeEmail(),
  body('password')
    .exists()
    .withMessage(i18n.__("user.password_required"))
    .notEmpty()
    .withMessage(i18n.__("user.password_filled"))
];
