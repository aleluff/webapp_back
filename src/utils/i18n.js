const path = require('path')
const {I18n} = require('i18n')

const i18n = new I18n({
  defaultLocale: 'fr',
  locales: ['fr', 'en'],
  fallbacks: 'en',
  objectNotation: true,
  syncFiles: true,
  directory: path.join(__dirname, "..", 'locales')
})

module.exports = i18n