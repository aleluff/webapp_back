const {multipleColumnSet} = require('./helpers');
const mysql = require("promise-mysql").createPool({
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_DATABASE
});

class Model {

  constructor() {
    mysql.then((conn) => {
      this.bdd = conn
    });
  }

  async find(params = {}) {
    let sql = `SELECT * FROM ${this.tableName}`;

    if (!Object.keys(params).length) {
      return await this.bdd.query(sql);
    }

    const {columnSet, values} = multipleColumnSet(params)
    sql += ` WHERE ${columnSet}`;

    return await this.bdd.query(sql, [...values]);
  }

  async findOne(params) {
    const {columnSet, values} = multipleColumnSet(params)

    const sql = `SELECT * FROM ${this.tableName}
        WHERE ${columnSet}`;

    const result = await this.bdd.query(sql, [...values]);

    // return back the first row (user)

    return result[0];
  }

  async update(params, id) {
    const {columnSet, values} = multipleColumnSet(params)

    const sql = `UPDATE ${this.tableName} SET ${columnSet} WHERE id = ?`;

    return await this.bdd.query(sql, [...values, id]);
  }

  async delete(id) {
    const sql = `DELETE FROM ${this.tableName}
        WHERE id = ?`;
    const result = await this.bdd.query(sql, [id]);
    const affectedRows = result ? result.affectedRows : 0;

    return affectedRows;
  }
}

module.exports = Model;
