exports.multipleColumnSet = (object) => {
  if (typeof object !== 'object') {
    throw new Error('Invalid input');
  }

  const keys = Object.keys(object);
  const values = Object.values(object);

  let columnSet = keys.map(key => `${key} = ?`).join(', ');

  return {
    columnSet,
    values
  }
}

exports.passwordMinLength = 8;

const password = `^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@#-_$!%*?&]{${exports.passwordMinLength},}$`;

exports.passwordRegex = new RegExp(password);

