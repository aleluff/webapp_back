const UserModel = require('../objects/user/model');
const jwt = require('jsonwebtoken');

const auth = (...roles) => {
  return async function (req, res, next) {
    const authHeader = req.headers.authorization;
    const bearer = 'Bearer ';

    if (!authHeader || !authHeader.startsWith(bearer)) {
      return res.error(401, 'auth.no_credential_sent');
    }

    const token = authHeader.replace(bearer, '');
    const secretKey = process.env.SECRET_JWT || "";

    // Verify Token
    let decoded;
    try {
      decoded = jwt.verify(token, secretKey);
    } catch (err) {
      return res.error(401, 'auth.jwt_expired');
    }

    const user = await UserModel.findOne({id: decoded.user_id});
    if (!user) {
      return res.error(401, 'auth.authentication_failed');
    }

    // check if the current user is the owner user
    const ownerAuthorized = req.params.id == user.id;

    // if the current user is not the owner and
    // if the user role don't have the permission to do this action.
    // the user will get this error
    if (!ownerAuthorized && roles.length && !roles.includes(user.role)) {
      // return res.status(401).send(`Unauthorized, you must be ${roles[0]}`);
      return res.error(401, "auth.unauthorized_role", { admin_role: roles[0] });
    }

    // if the user has permissions
    req.currentUser = user;
    next();
  }
}

module.exports = auth;
